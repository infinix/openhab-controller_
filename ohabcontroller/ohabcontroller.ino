

#include "buttoncontroller.h"
#include "lightcontroller.h"
#include <Ethernet.h>
#include <PubSubClient.h>
#include <SPI.h>
#include "lightbundle.h"

/*network information*/
IPAddress ip(192, 168, 0, 14);
IPAddress server(192, 168, 0, 178);
byte mac[]    = {  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
EthernetClient ethClient;
PubSubClient client(ethClient);
char buf[3];

int last_attempt;
const int retry_connect_attempt = 5000;

const int nrOfLightchannels = 4;
LightBundle* bundle[nrOfLightchannels];


void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  Serial.println();
  for(int i = 0;i<nrOfLightchannels;i++){
    char topicbuf[80];
    sprintf(topicbuf,"%s%s",bundle[i]->lighttopic,"light");
    Serial.println(topicbuf);
    if(strcmp(topic,topicbuf) == 0){
      bundle[i]->lightController->setState(payload[0]-48);
      sprintf(buf,"%d",bundle[i]->lightController->getState());
      if(client.connected()){
        char topicbuf[80];
        sprintf(topicbuf,"%s%s",bundle[i]->lighttopic,"lightstate");
        client.publish(topicbuf,buf);
      }
    }
  }

}


void reconnect() {
  
  if ((!client.connected()) &&  retry_connect_attempt < (millis () - last_attempt )) {
    last_attempt = millis();
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("arduinoClient")) {
      Serial.println("connected");
      for(int i = 0;i<nrOfLightchannels;i++){
        char topicbuf[80];
        sprintf(topicbuf,"%s%s",bundle[i]->lighttopic,"light");
        client.subscribe(topicbuf);
      }
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
    }
  }
}


void setup() {
  // put your setup code here, to run once:
  Serial.begin(57600);
    while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  Serial.println("Main light controller version 0.1");
  client.setServer(server,1883);
  client.setCallback(callback);
  Ethernet.begin(mac,ip);
  bundle[0] = new LightBundle(35,53,"/myhouse/mbedroom/");
  bundle[1] = new LightBundle(33,51,"/myhouse/livingroom/");
  bundle[2] = new LightBundle(31,49,"/myhouse/hall/");
  //bundle[3] = new LightBundle(46,47,"/myhouse/babyroom/");
  bundle[3] = new LightBundle(37,47,"/myhouse/diningroom/");
  //bundle[5] = new LightBundle(42,37,"/myhouse/bathroom/");
  //bundle[6] = new LightBundle(40,41,"/myhouse/bathroomradiator/");
  //bundle[7] = new LightBundle(38,39,"/myhouse/bathroomheater/");
}

void loop() {
    if (!client.connected()) {
    reconnect();
  }
  for(int i = 0;i<nrOfLightchannels;i++){
   ButtonController* button = bundle[i]->buttonController;
   LightController* light = bundle[i]->lightController;
     if(button->update() == BTNPRESSED){
      light->switchState();
      sprintf(buf,"%d",light->getState());
      if(client.connected()){
        char topicbuf[80];
        sprintf(topicbuf,"%s%s",bundle[i]->lighttopic,"lightstate");
        client.publish(topicbuf,buf);
      }
    }
   light->update();
  }
  client.loop();
}



