#ifndef Buttoncontroller_h
#define Buttoncontroller_h
#include "Arduino.h"

enum buttonState{
  BTNNOTPRESSED,
  BTNPRESSED,
  BTNLONGPRESSED
};

class ButtonController
{
  public:
    ButtonController(int pin);
    buttonState update();
   private:
    int _pin;
    int _lastSwitchState;
    int _lastDebouncetime;
    int _switchState;
    boolean _isPressed;
    buttonState _pressType;
    
};
#endif
