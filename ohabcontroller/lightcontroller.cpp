#include "Arduino.h"
#include "lightcontroller.h"

LightController::LightController(int pin,boolean dimmable){
  _dimmable = dimmable;
  _pin = pin;
  _requestedState =0;
  _currentState=0;
  pinMode(_pin,OUTPUT);
}

void LightController::update(){
  if(_currentState == _requestedState){
    return;
  }

  if (_requestedState > 0){
    
    digitalWrite(_pin,HIGH);
    _currentState=255;
  }
  else
  {
    digitalWrite(_pin,LOW);
    _currentState=0;
  }
}

void LightController::switchState(){
  if(_requestedState ==0){
    _requestedState = 255;
  }
  else{
    _requestedState = 0;
  }
}

void LightController::setState(int state){
  if(state>0){
    _requestedState = 255;
  }
  else{
    _requestedState = 0;
  }
}

int LightController::getState(){
  if(_requestedState> 0){
    return 1;
  }
  else{
    return 0;
  }
}


