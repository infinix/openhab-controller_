#ifndef lightbundle_h
#define lightbundle_h
#include "Arduino.h"
#include "lightcontroller.h"
#include "buttoncontroller.h"

class LightBundle
{
  public:
    LightController* lightController;
    ButtonController* buttonController;
    char const * lighttopic;
    LightBundle(int lightpin,int buttonpin,char const * topic);
    
    
};


#endif
