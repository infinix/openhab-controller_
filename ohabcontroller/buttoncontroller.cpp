#include "Arduino.h"
#include "buttoncontroller.h"

const int DEBOUNCEDELAY = 10;

ButtonController::ButtonController(int pin)
{

  this->_pin = pin;
  pinMode(pin,INPUT_PULLUP);
  _lastSwitchState=HIGH;
  Serial.println(this->_pin);
}

buttonState ButtonController::update()
{
  int reading = digitalRead(_pin);
//  Serial.print("reading from ");
//  Serial.print(_pin);
//  Serial.print(" got: ");
//  Serial.println(reading);
  
  if(reading != _lastSwitchState)
  {

    _lastDebouncetime = millis();
  }
  _lastSwitchState = reading; 
  if ((millis() - _lastDebouncetime) > DEBOUNCEDELAY)
  {
   if(reading!=_switchState){
    
    _switchState = reading;
    if(_switchState == LOW){
      _isPressed = true;
    }
    else
    {
      if(_isPressed){
        _isPressed = false;
        return BTNPRESSED;
      }
      else{
        _isPressed = false;
        return BTNNOTPRESSED;
      }
      
     
    }
   }
  }

  return BTNNOTPRESSED;
}



