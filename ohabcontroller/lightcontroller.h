#ifndef Lightcontroller_h
#define Lightcontroller_h
#include "Arduino.h"


class LightController
{
  public:
    //LightController(int pin);
    LightController(int pin,boolean dimmable);
    void update();
    void switchLight();
    void switchState();
    void setState(int state);
    int getState();
   private:
    int _pin;
    int _currentState;
    int _requestedState;
    boolean _dimmable;


    
    
};
#endif
